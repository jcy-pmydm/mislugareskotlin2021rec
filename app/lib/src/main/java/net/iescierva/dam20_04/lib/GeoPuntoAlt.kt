package net.iescierva.dam20_04.lib

class GeoPuntoAlt(var altura: Double, latitud: Double, longitud: Double) :
    GeoPunto(latitud, longitud) {
    override var latitud = 0.0
    override var longitud = 0.0
    var distanciaNueva = 0.0
    var punto1 = GeoPunto(latitud, longitud)
    var punto1Alt = GeoPuntoAlt(altura, latitud, longitud)
    override fun distancia(punto: GeoPunto): Double {
        distanciaNueva =
            Math.sqrt(punto1.distancia(punto1Alt) * punto1.distancia(punto1Alt) + punto1Alt.altura)
        return super.distancia(punto)
    }

    //diferencia_alturas = alturaActual - alturaAnterior;
    fun comprobarAltura() {
        if (altura < -2000 || altura > 10000) {
        }
    }
}
