package net.iescierva.dam20_04.lib

import java.util.*

open class GeoPunto @JvmOverloads constructor(
    open val latitud: Double = 0.0,
    open val longitud: Double = 0.0
) {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val geoPunto = o as GeoPunto
        return java.lang.Double.compare(
            geoPunto.latitud,
            latitud
        ) == 0 && java.lang.Double.compare(geoPunto.longitud, longitud) == 0
    }

    override fun hashCode(): Int {
        return Objects.hash(latitud, longitud)
    }

    open fun distancia(punto: GeoPunto): Double {
        val RADIO_TIERRA = 6371000.0 // en metros
        val dLat = Math.toRadians(latitud - punto.latitud)
        val dLon = Math.toRadians(longitud - punto.longitud)
        val lat1 = Math.toRadians(punto.latitud)
        val lat2 = Math.toRadians(latitud)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                Math.cos(lat1) * Math.cos(lat2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return c * RADIO_TIERRA
    }

    override fun toString(): String {
        return ("GeoPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}')
    }

    companion object {
        var SIN_POSICION = GeoPunto(0.0, 0.0)
    }
}