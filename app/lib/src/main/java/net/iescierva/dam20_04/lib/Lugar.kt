package net.iescierva.dam20_04.lib

class Lugar @JvmOverloads constructor(
    nombre: String? = "",
    direccion: String? = "",
    latitud: Double = 0.0,
    longitud: Double = 0.0,
    foto: String? = "",
    tipo: TipoLugar? = TipoLugar.OTROS,
    telefono: Int = 0,
    url: String? = "",
    comentario: String? = "",
    valoracion: Int = 0
) {
    private val nombre: String?
    private val direccion: String?
    private val posicion: GeoPunto
    private val tipo: TipoLugar?
    private val foto: String?
    private val telefono: Int
    private val url: String?
    private val comentario: String?
    private val fecha: Long
    private val valoracion: Float

    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    constructor(
        nombre: String?,
        direccion: String?,
        p: GeoPunto,
        foto: String?,
        tipo: TipoLugar?,
        telefono: Int,
        url: String?,
        comentario: String?,
        valoracion: Int
    ) : this(
        nombre, direccion, p.latitud, p.longitud, foto,
        tipo, telefono, url, comentario, valoracion
    ) {
    }

    init {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto(latitud, longitud)
        this.foto = foto
        this.tipo = tipo
        this.nombre = nombre
        this.direccion = direccion
        this.telefono = telefono
        this.url = url
        this.comentario = comentario
        this.valoracion = valoracion.toFloat()
    }
}
