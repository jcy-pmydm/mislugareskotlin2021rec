package net.iescierva.dam20_04.lib

internal interface RepositorioLugares {
    fun get_element(id: Int): Lugar? //Devuelve el elemento dado su id
    fun add(lugar: Lugar?) //Añade el elemento indicado
    fun add_blank(): Int //Añade un elemento en blanco y devuelve su id
    fun delete(id: Int) //Elimina el elemento con el id indicado
    fun size(): Int //Devuelve el número de elementos
    fun update_element(id: Int, lugar: Lugar?) //Reemplaza un elemento
}